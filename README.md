# ReadMore

React component for collapsing and expanding a text

## Usage

To install it:

> npm install --save @gluedigital/read-more

To use it:

```javascript
import ReadMore from '@gluedigital/read-more'

// ...

<ReadMore
  value={text}
  delimiter={'\n'}
  maxExcerptLength='250'
  messageReadMore={messageReadMore}
  messageReadLess={messageReadLess}
/>
```

## Options

The following props can be used:

| Name | Type | Description |
|------|------|-------------|
| value | string | Text that you want to show |
| delimiter | string | Characters that delimit how far the text collapses |
| maxExcerptLength | string | Default number by which the text will collapse if you do not find the delimiter|
| messageReadMore | string | Message displayed when the text is collapsed |
| messageReadLess | string | Message displayed when the text is expanded |

**Note**
The delimiter is replaced by a blank space when the text expands

## Developing

This package uses [nwb](https://github.com/insin/nwb) for the build. Take a look at their documentation for more info.

TL;DR: after installing nwb, just do `npm start` to open the dev environment.
