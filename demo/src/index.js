import React, {Component} from 'react'
import {render} from 'react-dom'

import ReadMore from '../../src'
import '../../css/readMore.css'

class Demo extends Component {
  render() {
    const text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's \n standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    const messageReadMore = "Read more"
    const messageReadLess = "Read less"
    return <div>
      <h1>readMore Demo</h1>
      <ReadMore
        value={text}
        delimiter={'\n'}
        maxExcerptLength='200'
        messageReadMore={messageReadMore}
        messageReadLess={messageReadLess}
      />
    </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))
