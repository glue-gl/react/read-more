import React, {Component} from 'react'
import PropTypes from 'prop-types'

export class ReadMore extends React.Component {

  static propTypes = {
    value: PropTypes.string.isRequired,
    maxExcerptLength: PropTypes.number.isRequired,
    messageReadMore: PropTypes.object.isRequired,
    messageReadLess: PropTypes.object.isRequired,
    delimiter: PropTypes.string
  }

  constructor (props) {
    super(props)
    this.state = { isOpen: false}
  }

  toggleOpen = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render = () => {
    const messageReadMore = this.props.messageReadMore
    const messageReadLess = this.props.messageReadLess
    const value = this.props.value
    const maxExcerptLength = this.props.maxExcerptLength

    const lineBreak = value.indexOf(this.props.delimiter)
    const excerptLength = (lineBreak > -1 && lineBreak < maxExcerptLength) ? lineBreak : maxExcerptLength
    const fullView = this.state.isOpen || excerptLength > value.length
    const text = fullView ? value.replace(this.props.delimiter, ' ')
      : value.substr(0, excerptLength) + '...'

    const message = !this.state.isOpen ? messageReadMore : messageReadLess

    const readMore = (excerptLength < value.length) && <div className="read-more-button"
        onClick={this.toggleOpen}>
        {message}
      </div>

    let readMoreClasses = 'readMore'
    if (this.state.isOpen) readMoreClasses += ' extended'

    return (
      <div className={readMoreClasses}>
        <span>{text}</span>
        {readMore}
      </div>
    )
  }
}

export default ReadMore
